import jsonschema
from django.core.exceptions import ValidationError
from django.core.validators import BaseValidator

FILTERS_SCHEMA = {
    "schema": "http://json-schema.org/draft-07/schema#",
    "type": "object",
    "properties": {
        "operator_code__in": {"type": "array", "items": {"type": "string"}, "uniqueItems": True, "default": []},
        "tag__in": {"type": "array", "items": {"type": "string"}, "uniqueItems": True, "default": []},
    },
}


class JSONSchemaValidator(BaseValidator):
    def compare(self, value, schema):
        try:
            jsonschema.validate(value, schema)
            for k in value:
                if k not in FILTERS_SCHEMA["properties"].keys():
                    raise ValidationError(
                        "%(value)s failed JSON schema check - unknown key %(key)s", params={"value": value, "key": k}
                    )
        except jsonschema.exceptions.ValidationError:
            raise ValidationError("%(value)s failed JSON schema check - wrong json format", params={"value": value})
