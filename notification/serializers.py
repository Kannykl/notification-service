from rest_framework import serializers

from notification.models import (
    Client,
    Mail,
    Message,
    Tag,
)


class ClientSerializer(serializers.ModelSerializer):
    tags = serializers.PrimaryKeyRelatedField(queryset=Tag.objects, allow_null=True, write_only=True, many=True)
    tag_names = serializers.SlugRelatedField(slug_field="name", source="tags", read_only=True, many=True)

    class Meta:
        model = Client
        fields = "__all__"


class MailSerializer(serializers.ModelSerializer):
    class Meta:
        model = Mail
        fields = "__all__"


class TagSerializer(serializers.ModelSerializer):
    class Meta:
        model = Tag
        fields = "__all__"


class MessageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Message
        fields = "__all__"
