from django.db.models import (
    Count,
    Q,
    QuerySet,
)

from notification.models import Mail


def add_stats_info_to_mails(queryset: QuerySet[Mail]) -> QuerySet[Mail]:
    """Add statistic info about messages count group by status"""
    queryset = queryset.annotate(
        created_messages=Count("messages", filter=Q(status="CREATED")),
        sent_messages=Count("messages", filter=Q(status="SENT")),
        failed_messages=Count("messages", filter=Q(status="FAILED")),
    )

    return queryset


def get_clients_filters_query(filters):
    """Make Q() object from json filters"""
    q_state = Q()
    for f in filters:
        q_state |= Q(**f)

    return q_state
