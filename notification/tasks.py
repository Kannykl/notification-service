from functools import reduce

import requests
from django.db.models import Q
from django.utils import timezone
from loguru import logger

from config.celery import app
from config.settings import (
    MESSAGE_SEND_API_ENDPOINT,
    MESSAGE_SEND_API_TOKEN,
    SEND_MESSAGE_API_TIMEOUT,
)
from notification.models import (
    Client,
    Mail,
    Message,
)
from notification.services import get_clients_filters_query


@app.task(name="send_mails")
def send_mails():
    """Check for active mails and send message to clients every minute."""
    active_mails = Mail.objects.filter(started_on__lte=timezone.now(), ended_on__gt=timezone.now())
    clients_filters = get_clients_filters_query(active_mails.values_list("client_filter", flat=True))
    receivers = Client.objects.filter(clients_filters)
    messages = Message.objects.filter(receiver__in=receivers).select_related("mail", "receiver")

    update_batch_size: int = 1000

    session = requests.session()
    session.headers.update({"bearerAuth": MESSAGE_SEND_API_TOKEN})

    for message in messages:
        try:
            response = session.post(
                f"{MESSAGE_SEND_API_ENDPOINT}/{message.id}",
                json={"id": message.id, "phone": message.receiver.phone, "message": message.mail.message},
                timeout=SEND_MESSAGE_API_TIMEOUT,
            )

            if not response.ok:
                logger.warning(f"Message {message.id} has failed with response status {response.status_code}")
                message.status = "FAILED"

        except requests.exceptions.Timeout:
            logger.warning(f"Message API service works too slow, abort sending message {message.id}")
            message.status = "FAILED"

        except Exception as e:
            logger.warning(f"Sending message failed cause {str(e)}")
            message.status = "FAILED"

        else:
            message.status = "ACCEPTED"
            logger.info(f"Message {message.id} is sent successfully")

    Message.objects.bulk_update(messages, ["status"], batch_size=update_batch_size)
