from django.core.validators import RegexValidator
from django.db import models

from utils.validators import (
    FILTERS_SCHEMA,
    JSONSchemaValidator,
)


class BaseModel(models.Model):
    started_on = models.DateTimeField(auto_now_add=True, verbose_name="Дата и время отправки")

    class Meta:
        abstract = True


class Mail(BaseModel):
    message = models.TextField(verbose_name="Текст сообщения для доставки клиенту")
    clients_filter = models.JSONField(
        default=dict,
        validators=[JSONSchemaValidator(limit_value=FILTERS_SCHEMA)],
        null=True,
        verbose_name="Фильтр свойств клиентов, на которых должна быть произведена рассылка ",
    )
    ended_on = models.DateTimeField(verbose_name="Дата и время окончания рассылки")

    class Meta:
        verbose_name = "Рассылка, в рамках которой отправляются сообщения клиентам"


class Tag(models.Model):
    name = models.CharField(max_length=255, verbose_name="Тэг")

    class Meta:
        verbose_name = "Тэг клиента"


class Client(models.Model):
    phone_number = models.CharField(
        max_length=11,
        unique=True,
        validators=[
            RegexValidator(
                regex=r"^7\d{10}$",
                message="Телефон должен быть в формате 7XXXXXXXXXX",
            ),
        ],
    )
    operator_code = models.CharField(max_length=10)
    tags = models.ManyToManyField(to=Tag, related_name="clients", verbose_name="Тэги клиента")
    timezone = models.CharField(max_length=100)

    def __str__(self):
        return self.phone_number

    class Meta:
        verbose_name = "Клиент, которому отправлются сообщения"


class Message(BaseModel):
    status_choices = (
        ("CREATED", "message is created"),
        ("SENT", "message is sent"),
        ("FAILED", "message is failed while sending"),
    )
    mail = models.ForeignKey(
        Mail,
        related_name="messages",
        on_delete=models.CASCADE,
        verbose_name="Рассылка в рамках, которой отправлено данное сообщение",
    )
    receiver = models.ForeignKey(
        Client, related_name="messages", on_delete=models.CASCADE, verbose_name="Клиент, которому отправлено сообщение"
    )
    status = models.CharField(
        max_length=32, default="CREATED", choices=status_choices, db_index=True, verbose_name="Статус сообщения"
    )

    class Meta:
        verbose_name = "Сообщения, отправляемые в рамках рассылки клиентам"
