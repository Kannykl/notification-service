import functools

from prometheus_client import Counter


request_counter_metric = Counter(
    name='request_counter',
    documentation='Счетчик запросов',
    labelnames=['method', 'endpoint'],
)


def prometheus_wraps(func):
    @functools.wraps
    def wrapper_func(*args, **kwargs):
        request = args[0]
        request_counter_metric.labels(method="GET", path=request.path)
        result = func(*args, **kwargs)
        return result

    return wrapper_func
