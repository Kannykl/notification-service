from rest_framework import viewsets

from notification.models import (
    Client,
    Mail,
    Message,
    Tag,
)
from notification.serializers import (
    ClientSerializer,
    MailSerializer,
    MessageSerializer,
    TagSerializer,
)
from notification.services import add_stats_info_to_mails


class ClientModelViewSet(viewsets.ModelViewSet):
    queryset = Client.objects.all()
    serializer_class = ClientSerializer


class TagsModelViewSet(viewsets.ModelViewSet):
    queryset = Tag.objects.all()
    serializer_class = TagSerializer


class MessagesModelViewSet(viewsets.ModelViewSet):
    queryset = Message.objects.all()
    serializer_class = MessageSerializer


class MailModelViewSet(viewsets.ModelViewSet):
    queryset = Mail.objects.all()
    serializer_class = MailSerializer

    def get_queryset(self):
        report_stats = self.request.query_params.get("report-stats", False)
        queryset = super().get_queryset()

        if report_stats:
            queryset = add_stats_info_to_mails(queryset)

        return queryset
