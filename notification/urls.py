from django.urls import (
    include,
    path,
)
from drf_yasg import openapi
from drf_yasg.views import get_schema_view
from rest_framework.permissions import AllowAny
from rest_framework.routers import DefaultRouter

from . import views

schema_view = get_schema_view(
    openapi.Info(
        title="Notification Service API",
        default_version="v1",
        description="API docs",
        terms_of_service="",
        contact=openapi.Contact(email="admin@google.com"),
        license=openapi.License(name="BSD License"),
    ),
    public=True,
    permission_classes=[AllowAny],
)


router = DefaultRouter()
router.register(r"clients", views.ClientModelViewSet, basename="clients")
router.register(r"mails", views.MailModelViewSet, basename="mails")
router.register(r"messages", views.MessagesModelViewSet, basename="messages")
router.register(r"tags", views.TagsModelViewSet, basename="tags")

urlpatterns = [
    path("", include(router.urls)),
    path('docs/', schema_view.with_ui('swagger', cache_timeout=0), name='schema-swagger-ui'),
    path('prometheus/', include('django_prometheus.urls'))
]
