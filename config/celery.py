from __future__ import absolute_import, unicode_literals
import os

import django
from celery import Celery
from celery.schedules import crontab

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "config.settings")
django.setup()
app = Celery("config", include=["notification.tasks"])

app.config_from_object("django.conf:settings", namespace="CELERY")
app.autodiscover_tasks()

app.conf.beat_schedule = {
    "every-minute-send-mails": {
        "task": "send_mails",
        "schedule": crontab(minute="*", hour="*"),
    }
}
