# Веб-сервер
FROM python:3.10-slim-buster

ENV TZ=Europe/Moscow

RUN apt-get update && \
    apt-get install -y

WORKDIR /opt/app

COPY requirements.txt .

RUN pip install  --upgrade pip --no-cache -r requirements.txt

COPY . .
