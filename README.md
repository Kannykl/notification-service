# Сервис уведомлений



## Стэк

- python 3.10
- DRF
- Celery
- docker



## Установка

```
docker-compose up --build
```

## Quick Start

localhost:8001/api/docs/ OpenAPI документация по API

Каждую минуту отрабатывает функция send_mails, которая проверяет рассылки и отправляет нужные.

